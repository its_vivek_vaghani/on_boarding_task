import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xffFFFFFF);
  static const black = Color(0xff000000);
  static const buttonTextColor = Color(0xff319795);
  static const kE6FFFA = Color(0xffE6FFFA);
  static const k319795 = Color(0xff319795);
  static const k3182CE = Color(0xff3182CE);
  static const k2D3748 = Color(0xff2D3748);
  static const kF7FAFC = Color(0xffF7FAFC);
  static const k000033 = Color(0xff000033);
  static const kCBD5E0 = Color(0xffCBD5E0);
  static const k81E6D9 = Color(0xff81E6D9);
  static const k4A5568 = Color(0xff4A5568);
  static const kEBF4FF = Color(0xffEBF4FF);
  static const k718096 = Color(0xff718096);
  static const k000029 = Color(0xff000029);
}