import 'package:demo_task/core/theme/app_color.dart';
import 'package:demo_task/core/widgets/container_button.dart';
import 'package:flutter/material.dart';

class CommonHeader extends StatelessWidget {
  final bool viewButton;

  const CommonHeader({super.key, required this.viewButton});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                AppColors.k319795,
                AppColors.k3182CE,
              ],
            ),
          ),
        ),
        Container(
          height: 62,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(12.0),
              bottomRight: Radius.circular(12.0),
            ),
            boxShadow: [
              BoxShadow(
                color: AppColors.k000029.withOpacity(0.2),
                offset: const Offset(0, 3),
                blurRadius: 6,
              )
            ],
          ),
          alignment: Alignment.centerRight,
          padding: const EdgeInsets.only(
            right: 16,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (viewButton) ...[
                const Text(
                  "Jetzt Klicken",
                  style: TextStyle(
                    letterSpacing: 0.84,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Color(0xFF4A5568),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                const ContainerButton(
                  title: "Kostenlos Registrieren",
                ),
                const SizedBox(
                  width: 20,
                ),
              ],
              const Text(
                'Login',
                style: TextStyle(
                  letterSpacing: 0.84,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                  color: AppColors.k319795,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
