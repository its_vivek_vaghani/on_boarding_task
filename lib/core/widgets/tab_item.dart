import 'package:demo_task/core/theme/app_color.dart';
import 'package:flutter/material.dart';

class TabItem extends StatelessWidget {
  const TabItem({
    super.key,
    this.onPressed,
    required this.index,
    required this.isSelected,
    required this.title,
  });

  final void Function()? onPressed;
  final int index;
  final bool isSelected;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 40,
        width: 160,
        decoration: BoxDecoration(
            color: isSelected ? AppColors.k81E6D9 : AppColors.white,
            border: isSelected
                ? null
                : Border.all(
                    color: AppColors.kCBD5E0,
                    width: 1,
                  ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(index == 0 ? 12 : 0),
              topRight: Radius.circular(index == 2 ? 12 : 0),
              bottomLeft: Radius.circular(index == 0 ? 12 : 0),
              bottomRight: Radius.circular(index == 2 ? 12 : 0),
            )),
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
            fontSize: 14,
            letterSpacing: 0.84,
            color: isSelected ? AppColors.kE6FFFA : AppColors.k319795,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
