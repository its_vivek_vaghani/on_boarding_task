import 'package:demo_task/core/theme/app_color.dart';
import 'package:flutter/material.dart';

class ContainerGradientButton extends StatelessWidget {
  final String text;
  final void Function()? onPressed;
  const ContainerGradientButton({
    super.key,
    required this.text,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            gradient: const LinearGradient(
              colors: [
                AppColors.k319795,
                AppColors.k3182CE,
              ],
            ),
            borderRadius: BorderRadius.circular(12)),
        alignment: Alignment.center,
        child: Text(
          text,
          style: const TextStyle(
              fontSize: 14,
              color: AppColors.kE6FFFA,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.84),
        ),
      ),
    );
  }
}
