class AppImages {
  const AppImages._();

  /*----------------------------- Onboard Images ---------------------------*/
  static const onboardAgreementImage = 'assets/images/onboard_agreement_image.png';
  static const onboardProfileDataImage = 'assets/images/onboard_profile_data.png';
  static const onboardTaskImage = 'assets/images/onboard_task_image.png';
  static const onboardPersonalFileImage = 'assets/images/onboard_personal_file_image.png';
  static const onboardAboutMeImage = 'assets/images/onboard_about.png';
  static const onboardSwipeProfileImage = 'assets/images/onboard_swipe_profile_image.png';
  static const onboardBusinessDealImage = 'assets/images/onboard_business_deal_images.png';
  static const onboardJobOfferImage = 'assets/images/onboard_job_offer_image.png';
}