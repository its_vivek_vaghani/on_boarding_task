class AppConstants {
  const AppConstants._();

  /*----------------------------- Onboard Images ---------------------------*/
  static const onBoardTabItems =  [
    "Arbeitnehmer",
    'Arbeitgeber',
    'Temporärbüro',
  ];
}