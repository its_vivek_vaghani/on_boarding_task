import 'package:flutter/material.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget webView;
  final Widget? mobileView;

  const ResponsiveLayout({
    super.key,
    required this.webView,
    this.mobileView,
  });

  static bool isMobileView(BuildContext context) {
    return MediaQuery.of(context).size.width <= 560;
  }

  static bool isWebView(BuildContext context) {
    return MediaQuery.of(context).size.width > 560;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 600) {
          return webView;
        } else {
          return mobileView ?? webView;
        }
      },
    );
  }
}
