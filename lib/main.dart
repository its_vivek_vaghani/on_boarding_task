import 'package:demo_task/core/theme/app_theme.dart';
import 'package:demo_task/core/utills/app_routes.dart';
import 'package:demo_task/presentation/on_boarding/on_boarding.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Demo Task',
      theme: AppTheme.defaultTheme,
      initialRoute: AppRoutes.onBoarding,
      routes: {
        AppRoutes.onBoarding: (context) => const Onboarding(),
      },
    );
  }
}


