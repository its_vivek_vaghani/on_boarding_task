import 'package:flutter/material.dart';

class OnboardTopSectionClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);
    path.quadraticBezierTo(
      size.width * 0.25,
      size.height * 1.0,
      size.width * 0.5,
      size.height * 0.95,
    );
    path.quadraticBezierTo(
      size.width * 0.75,
      size.height * 0.9,
      size.width,
      size.height * 0.90,
    );
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class OnboardTabClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);
    path.quadraticBezierTo(
      size.width * 0.25,
      size.height * 0.90,
      size.width * 0.5,
      size.height * 0.95,
    );
    path.quadraticBezierTo(
      size.width * 0.80,
      size.height * 1,
      size.width,
      size.height * 0.84,
    );
    path.lineTo(size.width, size.height * 0.08);
    path.quadraticBezierTo(
      size.width * 0.90,
      size.height * 0.05,
      size.width * 0.75,
      size.height * 0.10,
    );
    path.quadraticBezierTo(
      size.width * 0.45,
      size.height * 0.20,
      size.width * 0.22,
      size.height * 0.07,
    );
    path.quadraticBezierTo(
      size.width * 0.10,
      size.height * 0.00,
      0,
      0,
    );
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class OnboardBottomSectionClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);
    path.quadraticBezierTo(
      size.width * 0.25,
      size.height * 0.90,
      size.width * 0.5,
      size.height * 0.95,
    );
    path.quadraticBezierTo(
      size.width * 0.80,
      size.height * 1,
      size.width,
      size.height * 0.84,
    );
    path.lineTo(size.width, size.height * 0.25);
    path.quadraticBezierTo(
      size.width * 0.90,
      size.height * 0.05,
      size.width * 0.75,
      size.height * 0.10,
    );
    path.quadraticBezierTo(
      size.width * 0.45,
      size.height * 0.20,
      size.width * 0.22,
      size.height * 0.07,
    );
    path.quadraticBezierTo(
      size.width * 0.10,
      size.height * 0.00,
      0,
      0,
    );
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class CustomOvalClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, size.width - 50, size.height);
  }

  @override
  bool shouldReclip(covariant CustomClipper<Rect> oldClipper) {
    return false;
  }
}
