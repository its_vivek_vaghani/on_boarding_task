import 'package:demo_task/core/responsive_layout.dart';
import 'package:demo_task/core/theme/app_color.dart';
import 'package:demo_task/core/utills/app_images.dart';
import 'package:demo_task/core/utills/constants.dart';
import 'package:demo_task/core/widgets/common_header.dart';
import 'package:demo_task/core/widgets/container_gradient_button.dart';
import 'package:demo_task/core/widgets/tab_item.dart';
import 'package:demo_task/presentation/on_boarding/tab_view/tab_view_main.dart';
import 'package:demo_task/presentation/on_boarding/widgets/draw_clippers.dart';
import 'package:flutter/material.dart';

class Onboarding extends StatefulWidget {
  const Onboarding({super.key});

  @override
  State<Onboarding> createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  late ScrollController tabScrollController, pageScrollController;
  int curIndex = 0;
  bool showTopSection = false;

  @override
  void initState() {
    super.initState();
    tabScrollController = ScrollController();
    pageScrollController = ScrollController();
    pageScrollController.addListener(() {
      if (pageScrollController.offset > 325) {
        if (!showTopSection) {
          setState(() {
            showTopSection = true;
          });
        }
      } else {
        if (showTopSection) {
          setState(() {
            showTopSection = false;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            controller: pageScrollController,
            child: Column(
              children: [
                ResponsiveLayout.isMobileView(context)
                    ? SizedBox(
                        height: 700,
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            ClipPath(
                              clipper: OnboardTopSectionClipper(),
                              child: Container(
                                decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      AppColors.kEBF4FF,
                                      AppColors.kE6FFFA,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 85,
                                    ),
                                    const Text(
                                      'Deine Job\nwebsite',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 42,
                                        letterSpacing: 1.26,
                                        color: AppColors.k2D3748,
                                        height: 1.2,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Expanded(
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            left: -40,
                                            right: -110,
                                            top: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              AppImages.onboardAgreementImage,
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : ClipPath(
                        clipper: OnboardTopSectionClipper(),
                        child: Container(
                          height: 550,
                          padding: const EdgeInsets.only(top: 85),
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                AppColors.kEBF4FF,
                                AppColors.kE6FFFA,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          child: Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 25.0),
                              child: ConstrainedBox(
                                constraints: const BoxConstraints(
                                  maxWidth: 800,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const Text(
                                            'Deine Job\nwebsite',
                                            style: TextStyle(
                                              fontSize: 42,
                                              letterSpacing: 1.26,
                                              color: AppColors.k2D3748,
                                              height: 1.2,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 40,
                                          ),
                                          ContainerGradientButton(
                                            text: 'Kostenlos Registrieren',
                                            onPressed: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 7,
                                      child: Center(
                                        child: Container(
                                          height: 350,
                                          width: 390,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                            image: DecorationImage(
                                              image: AssetImage(
                                                AppImages.onboardAgreementImage,
                                              ),
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                SizedBox(
                  height: ResponsiveLayout.isMobileView(context) ? 27 : 35,
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 40,
                    width: 520,
                    child: ListView.builder(
                      controller: tabScrollController,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      scrollDirection: Axis.horizontal,
                      itemCount: AppConstants.onBoardTabItems.length,
                      itemBuilder: (context, index) => TabItem(
                        index: index,
                        isSelected: index == curIndex,
                        title: AppConstants.onBoardTabItems[index],
                        onPressed: () {
                          var width = MediaQuery.of(context).size.width;
                          var offset = 520 - width;
                          if (width < 520) {
                            tabScrollController.animateTo(
                              index == 0
                                  ? 0
                                  : (index == 1 ? (offset / 2) : offset),
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          }
                          setState(() {
                            curIndex = index;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: ResponsiveLayout.isMobileView(context) ? 30 : 60,
                ),
                TabViewMain(index: curIndex),
                SizedBox(
                  height: ResponsiveLayout.isMobileView(context) ? 130 : 0,
                ),
              ],
            ),
          ),
          CommonHeader(
              viewButton:
                  ResponsiveLayout.isWebView(context) ? showTopSection : false),
          if (ResponsiveLayout.isMobileView(context))
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 90,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.k000033.withOpacity(0.25),
                      blurRadius: 3,
                      offset: const Offset(0, -1),
                    )
                  ],
                ),
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.only(
                  top: 24,
                  left: 20,
                  right: 20,
                ),
                child: ContainerGradientButton(
                  text: 'Kostenlos Registrieren',
                  onPressed: () {},
                ),
              ),
            )
        ],
      ),
    );
  }
}
