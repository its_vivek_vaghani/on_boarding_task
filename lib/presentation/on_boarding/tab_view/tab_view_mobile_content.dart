import 'package:demo_task/core/theme/app_color.dart';
import 'package:demo_task/core/utills/app_images.dart';
import 'package:demo_task/presentation/on_boarding/widgets/draw_clippers.dart';
import 'package:flutter/material.dart';

class TabViewMobileContent extends StatelessWidget {
  final int index;

  const TabViewMobileContent({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          index == 0
              ? 'Drei einfache Schritte\nzu deinem neuen Job'
              : index == 1
                  ? 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter'
                  : 'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
          textAlign: TextAlign.center,
          style: const TextStyle(
            height: 1.2,
            fontSize: 21,
            color: AppColors.k4A5568,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 225,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 40),
                  child: Image.asset(
                    AppImages.onboardProfileDataImage,
                    height: 144.55,
                    width: 219.56,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text(
                        "1",
                        style: TextStyle(
                          fontSize: 130,
                          height: 1,
                          fontWeight: FontWeight.normal,
                          color: AppColors.k718096,
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            index == 0
                                ? 'Erstellen dein Lebenslauf'
                                : 'Erstellen dein Unternehmensprofil',
                            style: const TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.47,
                              fontWeight: FontWeight.normal,
                              color: AppColors.k718096,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 400,
          child: Stack(
            fit: StackFit.expand,
            children: [
              ClipPath(
                clipper: OnboardTabClipper(),
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        AppColors.kE6FFFA,
                        AppColors.kEBF4FF,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 47.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        const Text(
                          "2",
                          style: TextStyle(
                            fontSize: 130,
                            height: 1,
                            fontWeight: FontWeight.normal,
                            color: AppColors.k718096,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Text(
                              index == 0
                                  ? 'Erstellen dein Lebenslauf'
                                  : index == 1
                                      ? 'Erstellen ein Jobinserat'
                                      : 'Erhalte Vermittlungs- angebot von Arbeitgeber',
                              style: const TextStyle(
                                fontSize: 16,
                                letterSpacing: 0.47,
                                fontWeight: FontWeight.normal,
                                color: AppColors.k718096,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 80.0),
                      child: Image.asset(
                        index == 0
                            ? AppImages.onboardTaskImage
                            : index == 1
                                ? AppImages.onboardAboutMeImage
                                : AppImages.onboardJobOfferImage,
                        height: 126.55,
                        width: 180.74,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(
          height: 360,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: -30,
                left: -65,
                child: ClipOval(
                  clipper: CustomOvalClipper(),
                  child: Container(
                    height: 360,
                    width: 360,
                    color: AppColors.kF7FAFC,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 56.0),
                    child: Row(
                      children: [
                        const Text(
                          "3",
                          style: TextStyle(
                            fontSize: 130,
                            height: 1,
                            fontWeight: FontWeight.normal,
                            color: AppColors.k718096,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            index == 0
                                ? 'Mit nur einem Klick bewerben'
                                : index == 1
                                    ? 'Wähle deinen neuen Mitarbeiter aus'
                                    : 'Vermittlung nach Provision oder Stundenlohn',
                            style: const TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.47,
                              fontWeight: FontWeight.normal,
                              color: AppColors.k718096,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Image.asset(
                      index == 0
                          ? AppImages.onboardPersonalFileImage
                          : index == 1
                              ? AppImages.onboardSwipeProfileImage
                              : AppImages.onboardBusinessDealImage,
                      height: 210,
                      width: 281,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
