import 'package:dashed_line/dashed_line.dart';
import 'package:flutter/material.dart';
import 'package:demo_task/core/theme/app_color.dart';
import 'package:demo_task/core/utills/app_images.dart';
import 'package:demo_task/presentation/on_boarding/widgets/draw_clippers.dart';
import 'package:flutter/material.dart';

class TabViewWebContent extends StatelessWidget {
  final int index;

  const TabViewWebContent({required this.index});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 420,
          left: 0,
          right: 0,
          child: ClipPath(
            clipper: OnboardBottomSectionClipper(),
            child: Container(
              height: 300,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    AppColors.kE6FFFA,
                    AppColors.kEBF4FF,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
            ),
          ),
        ),
        Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 800,
            ),
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Positioned(
                  left: 0,
                  top: 160,
                  child: Container(
                    height: 160,
                    width: 160,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 25,
                  bottom: 70,
                  child: Container(
                    height: 220,
                    width: 230,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 100,
                  top: 300,
                  child: SizedBox(
                    height: 270,
                    width: 375,
                    child: DashedLine(
                      path: drawPathArrowSecond(270.0, 375.0),
                      color: const Color(0xFF4A5568),
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Positioned(
                  left: 135,
                  top: 650,
                  child: SizedBox(
                    height: 270,
                    width: 395,
                    child: DashedLine(
                      path: drawPathArrowFirst(270.0, 395.0),
                      color: AppColors.k4A5568,
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        index == 0
                            ? 'Drei einfache Schritte\nzu deinem neuen Job'
                            : index == 1
                                ? 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter'
                                : 'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          height: 1.2,
                          fontSize: 36,
                          color: AppColors.k4A5568,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 225,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 12),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                      "1",
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? 'Erstellen dein Lebenslauf'
                                              : 'Erstellen dein Unternehmensprofil',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: AppColors.k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Image.asset(
                                AppImages.onboardProfileDataImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            const Expanded(flex: 1, child: SizedBox())
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 400,
                        child: Row(
                          children: [
                            Expanded(
                              child: Image.asset(
                                index == 0
                                    ? AppImages.onboardTaskImage
                                    : index == 1
                                        ? AppImages.onboardAboutMeImage
                                        : AppImages.onboardJobOfferImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 47.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                        "2",
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? 'Erstellen dein Lebenslauf'
                                              : index == 1
                                                  ? 'Erstellen ein Jobinserat'
                                                  : 'Erhalte Vermittlungs- angebot von Arbeitgeber',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: AppColors.k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 360,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 80.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                      "3",
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: AppColors.k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? 'Mit nur einem Klick bewerben'
                                              : index == 1
                                                  ? 'Wähle deinen neuen Mitarbeiter aus'
                                                  : 'Vermittlung nach Provision oder Stundenlohn',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: AppColors.k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Image.asset(
                                index == 0
                                    ? AppImages.onboardPersonalFileImage
                                    : index == 1
                                        ? AppImages.onboardSwipeProfileImage
                                        : AppImages.onboardBusinessDealImage,
                                fit: BoxFit.contain,
                                height: 200,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

Path drawPathArrowFirst(double height, double width) {
  final path = Path()
    ..moveTo(
      width,
      0,
    )
    ..quadraticBezierTo(
        width * 0.90, height * 0.30, width * 0.62, height * 0.36)
    ..quadraticBezierTo(
        width * 0.25, height * 0.45, width * 0.18, height * 0.70)
    ..moveTo(width * 0.173, height * 0.72)
    ..lineTo(width * 0.16, height * 0.692)
    ..moveTo(width * 0.173, height * 0.72)
    ..lineTo(width * 0.20, height * 0.70);
  return path;
}

Path drawPathArrowSecond(double height, double width) {
  final path = Path()
    ..moveTo(
      0,
      0,
    )
    ..quadraticBezierTo(
        width * 0.10, height * 0.30, width * 0.38, height * 0.36)
    ..quadraticBezierTo(
        width * 0.75, height * 0.45, width * 0.82, height * 0.70)
    ..moveTo(width * 0.827, height * 0.72)
    ..lineTo(width * 0.84, height * 0.69)
    ..moveTo(width * 0.827, height * 0.72)
    ..lineTo(width * 0.80, height * 0.70);
  return path;
}