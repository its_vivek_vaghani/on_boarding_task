import 'package:demo_task/core/responsive_layout.dart';
import 'package:demo_task/presentation/on_boarding/tab_view/tab_view_mobile_content.dart';
import 'package:demo_task/presentation/on_boarding/tab_view/tab_view_web_content.dart';
import 'package:flutter/material.dart';

class TabViewMain extends StatelessWidget {
  final int index;

  const TabViewMain({required this.index, super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout.isMobileView(context)
        ? TabViewMobileContent(index: index)
        : TabViewWebContent(index: index);
  }
}


